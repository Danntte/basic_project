<?php 

namespace App\Http;

class Request
{
    protected $segments = [];
    protected $controller;
    protected $method;

    public function __construct()
    {
        $this->segments = explode('/', $_SERVER['REQUEST_URI']);

        $this->setController();
        $this->setMethod();
    }

    public function setController()
    {
        $this->controller = empty($this->segments[1]) ? 'home' : $this->segments[1];
        $this->controller = 'App\Http\Controllers\\' .  ucfirst($this->controller);
    }

    public function setMethod()
    {
        $this->method = empty($this->segments[2]) ? 'index' : $this->segments[2];
    }

    public function send()
    {
        $controller = $this->controller . "Controller";
        $method = $this->method;

        $response = call_user_func( [new $controller , $method] );

        $response->send();
    }
}

?>