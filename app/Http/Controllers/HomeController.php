<?php

namespace App\Http\Controllers;

use function App\view;

class HomeController
{
    public function index()
    {
        return view('home');
    }
}

?>